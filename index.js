function cancelRedirect() {
    window.history.back(-1)
    setTimeout(function(){ 
        if (window.location.href == window.location.href) {
            window.close
        }
    }, 500);
}

var urlParams = new URLSearchParams(window.location.search)

// go to the generator if the request is invalid
if (urlParams.get('url') == null || urlParams.get('img') == null) {
    window.location.href = 'gen'
}

// get the URL
var targetURL = urlParams.get('url')

// ensure it's a valid URL
if (targetURL.substr(0,3) !== "htt") {
    var targetURL = "https://" + targetURL
}

// create an a tag to add targetURL to, in order to get the domain
var getDomainFake = document.createElement('a')
getDomainFake.setAttribute('href', targetURL)

// put the domain into the text
document.getElementById("redirectedTo").innerHTML = getDomainFake.hostname

document.getElementById("redirectedToImg").setAttribute('src', urlParams.get('img'))

// make arrow button work
document.getElementById("skipWait").setAttribute('onclick', 'window.location.replace("' + targetURL + '")')

// modified version of https://stackoverflow.com/a/31106382
var timeleft = 3;
var downloadTimer = setInterval(function(){
    timeleft--;
    document.getElementById("countdown").textContent = timeleft;
    if(timeleft <= 0)
        clearInterval(downloadTimer);},
1000)

setInterval(function(){
    window.location.replace(targetURL)
},3000)